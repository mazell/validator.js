/*
 * validate.js 0.1
 * Copyright (c) 2015 Mattias Eklund, @outboundlink
 */
(function(window, document, undefined) {
    function Validator(type) {
        this.Validators = {
            required: function(obj) {
                if (this.value.length < 1) {
                    obj.isValid = false;
                    this.value = "Vi saknar detta fält";
                }
            },
            email: function(obj) {
                if (!_validateEmail(this.value)) {
                    obj.isValid = false;
                    this.value = "Verkar inte vara en korrekt e-post";
                }
            },
            number: function() {
                //check if value is numeric
                this.value = "Bara siffror tack";
            }
        };

        function _validateEmail(em) {
            var pattern = /^[a-z\d]+(?:[\-\.\_][a-z\d]+)*[a-z\d]+@[\w\d]+(?:[-\.][a-z\d][a-z\d\-]*[a-z\d])*[a-z\d]+\.([a-z]{2,4})(\.([a-z]{2,4}))*$/i;
            return (pattern.test(em)) ? true : false;
        };
        return this.Validators[type];
    }
    var Field = function(validators) {
        this.hooks = [];
        for (i = 0; i < validators.length; i++) {
            this.hooks.push(new Validator(validators[i]));
        };
    };
    Field.prototype._getElementSelector = function(selector) {
        document.querySelectorAll(selector);
    };
    Field.prototype._addValidatorHook = function(hook) {
        this.hooks.push(hook);
    };
    /* INPUT VALIDATOR */
    var InputValidator = function(formSelector) {
        this.fields = [];
        this.validatorhooks = [];
        this.isValid = true;
        this.selFormID = "#" + document.querySelector(formSelector).id + ' ';
    };
    InputValidator.prototype.addFieldToValidate = function(selector, validators) {
        var el = document.querySelector(this.selFormID + selector);
        //console.log(el);
        el.validator = new Field(validators);
        this.fields.push(el);
    };
    InputValidator.prototype.validateFields = function() {
        //reset valid
        this.isValid = true;
        var allFields = this.fields;
        for (i = 0; i < allFields.length; i++) {
            var el = allFields[i];
            var fieldValidator = allFields[i].validator;
            for (x = 0; x < fieldValidator.hooks.length; x++) {
                fieldValidator.hooks[x].apply(el, [this]);
            }
        }
    }
    //set global object.
    window.InputValidator = InputValidator;
})(window, document);
// var validator = new InputValidator();
// validator.addFieldToValidate("#textinput", ['required']);
// validator.validateFields();
// console.log(validator.isValid);